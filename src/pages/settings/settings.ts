import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { logoutUser } from '../../reducers/auth';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public store: Store<AppState>) {
    
  }

  logout() {
    this.store.dispatch(logoutUser());
  }
}
