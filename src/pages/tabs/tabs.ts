import { Component } from '@angular/core';

import { SettingsPage } from '../settings/settings';
import { HomePage } from '../home/home';
import { TrackersPage } from '../trackers/trackers';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = TrackersPage;
  tab3Root = SettingsPage;

  constructor() {

  }
}
