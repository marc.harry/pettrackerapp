import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';

import { TrackersService } from './trackers.service';

import { GET_TRACKERS, SET_ACTIVE_TRACKER, storeTracker } from './trackers.reducers';

@Injectable()
export class TrackerEffects {
  constructor(
    private trackersService: TrackersService,
    private actions$: Actions,
    private storage: Storage
  ) { }

  @Effect() getTrackers$ = this.actions$
      .ofType(GET_TRACKERS)
      .switchMap(payload => this.trackersService.get()
        .map(res => storeTracker(res.json()))
        .catch(error => {
          console.log(error);
          return Observable.empty();
        })
      );

  @Effect() activeTracker$ = this.actions$
    .ofType(SET_ACTIVE_TRACKER)
    .map(action => {
        this.storage.set('activeTracker', action.payload)
        return Observable.empty();
    });
}