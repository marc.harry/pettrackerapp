import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AddTrackerPage } from './add-tracker/add-tracker';

import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';

import { getTrackers, setActiveTracker } from './trackers.reducers';

@Component({
  selector: 'page-trackers',
  templateUrl: 'trackers.html',
})
export class TrackersPage {
  trackers: any[];
  activeTracker: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public store: Store<AppState>) {
    this.store.select(s => s.trackers.trackers).subscribe(trackers => {
      this.trackers = trackers
    });
    this.store.select(s => s.trackers.activeTracker).subscribe(tracker => {
      this.activeTracker = tracker
    });
  }

  ionViewDidLoad() {
    this.store.dispatch(getTrackers());
  }

  addTracker() {
    this.navCtrl.push(AddTrackerPage);
  }

  setAsActiveTracker(tracker: any) {
    this.store.dispatch(setActiveTracker(tracker.id));
  }
}
