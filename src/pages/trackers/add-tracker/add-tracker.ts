import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-add-tracker',
  templateUrl: 'add-tracker.html',
})
export class AddTrackerPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
