import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Environment } from '../../environment/environment';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TrackersService {
    authorizationToken: string;

    constructor(private http: Http, private settings: Environment, private store: Store<AppState>) { 
        this.store.select(s => s.auth.token.access_token).subscribe(token => this.authorizationToken = token);
    }
    
    baseUrl() {
        return `${this.settings.env.apiEndpoint}api/pettrackers`
    }

    requestOpts(): RequestOptionsArgs {
        return {
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.authorizationToken}`
            })
        }
    }

    get(): Observable<Response> {
        return this.http.get(this.baseUrl(), this.requestOpts());
    }
}