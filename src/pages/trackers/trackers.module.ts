import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrackersPage } from './trackers';

import { AddTrackerPage } from './add-tracker/add-tracker';

import { TrackersService } from './trackers.service';

import { EffectsModule } from '@ngrx/effects';
import { TrackerEffects } from './trackers.effects';

@NgModule({
  declarations: [
    TrackersPage,
    AddTrackerPage
  ],
  providers: [
    TrackersService
  ],
  imports: [
    IonicPageModule.forChild(TrackersPage),
    EffectsModule.run(TrackerEffects)
  ],
  exports: [
    TrackersPage
  ],
  entryComponents: [
    AddTrackerPage,
    TrackersPage
  ]
})
export class TrackersPageModule {}
