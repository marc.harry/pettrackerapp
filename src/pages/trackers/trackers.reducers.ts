import { Action } from '@ngrx/store';

export const STORE_TRACKER  = 'STORE_TRACKER';
export const GET_TRACKERS  = 'GET_TRACKERS';
export const SET_ACTIVE_TRACKER = 'SET_ACTIVE_TRACKER';

export const getTrackers = (): Action => ({ type: GET_TRACKERS });
export const storeTracker = (payload: any): Action => ({ type: STORE_TRACKER, payload });
export const setActiveTracker = (payload: number): Action => ({ type: SET_ACTIVE_TRACKER, payload });

export interface ITrackerState { 
    trackers: any[];
    activeTracker: any;
    activeTrackerId: number;
 }

 const initialState: ITrackerState = {
     trackers: [],
     activeTracker: {},
     activeTrackerId: null
 }

export function trackers(state = initialState, action: Action) {
    switch (action.type) {
        case STORE_TRACKER:
            let activeTracker = null;
            if (state.activeTrackerId) {
                activeTracker = action.payload.find(t => t.id === state.activeTrackerId);
            }
            return Object.assign({}, state, {trackers: action.payload, activeTracker});
        case SET_ACTIVE_TRACKER:
            const tracker = state.trackers.find(t => t.id === action.payload);
            return Object.assign({}, state, {activeTracker: tracker, activeTrackerId: action.payload});
        default:
            return state;
    }
}