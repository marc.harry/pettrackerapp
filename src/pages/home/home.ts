import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { INCREMENT } from '../../reducers/counter';

import { 
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  LatLng,
  CameraPosition,
  MarkerOptions,
  Marker } from '@ionic-native/google-maps';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  counter: number = 0;

  constructor(public navCtrl: NavController, public store: Store<AppState>, public googleMaps: GoogleMaps) {
    this.store.select(s => s.counter).subscribe(counter => this.counter = counter);
  }

  ngAfterViewInit() {
    this.loadMap();
  }

  updateCounter() {
    this.store.dispatch({ type: INCREMENT });
  }

  loadMap() {
    // create a new map by passing HTMLElement
    let element: HTMLElement = document.getElementById('map');

    let map: GoogleMap = this.googleMaps.create(element);

    // listen to MAP_READY event
    // You must wait for this event to fire before adding something to the map or modifying it in anyway
    map.one(GoogleMapsEvent.MAP_READY).then(() => {
      console.log('Map is ready!');
      // Now you can add elements to the map like the marker
      map.setOptions({
        'controls': {
          'compass': true,
          'myLocationButton': true,
          'zoom': true
        }
      });

      // Get Users Lat Long Here.
      let ionic: LatLng = new LatLng(51.6214, -3.9436);

      map.addCircle({
        fillColor: '#FFFFF',
        center: ionic,
        radius: 100, // Meters
      }).then((circle) => {
        
      });

      // create CameraPosition
      let position: CameraPosition = {
        target: ionic,
        zoom: 14,
        tilt: 0
      };

      // move the map's camera to position
      map.moveCamera(position);

      // create new marker
      let markerOptions: MarkerOptions = {
        position: ionic,
        title: 'You are here'
      };

      map.addMarker(markerOptions)
        .then((marker: Marker) => {
          // Set a radius on the marker?
        });
    });
  }
}
