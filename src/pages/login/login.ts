import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { loginUser } from '../../reducers/auth';

import { RegisterPage } from '../register/register';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  loginDetails = {
    username: '',
    password: ''
  };
  

  constructor(public navCtrl: NavController, public store: Store<AppState>) {
  }

  performLogin() {
    // TODO: Change this to dispatch to action to perform login for now faking login
    this.store.dispatch(loginUser(this.loginDetails));
  }

  goToRegisterPage() {
    this.navCtrl.push(RegisterPage);
  }
}
