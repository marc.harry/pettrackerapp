import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import {PasswordValidation} from '../../validators/passwordValidator';

import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { registerUser } from '../../reducers/auth';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  registerForm: FormGroup;

  constructor(public navCtrl: NavController, public store: Store<AppState>, private fb: FormBuilder) {
    this.registerForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, {
      validator: PasswordValidation.MatchPassword
    });
  }

  registerUser() {
    const registerDetails = this.registerForm.value;
    this.store.dispatch(registerUser(registerDetails));
  }
}
