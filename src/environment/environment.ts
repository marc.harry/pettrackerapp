import { OpaqueToken, Injectable, Inject } from '@angular/core';

export let EnvVariables = new OpaqueToken('env.variables');

export interface IEnvironment {
    apiEndpoint: string;
    environmentName: string;
    ionicEnvName: string;

    client_id: string;
    grant_type: string;
    client_secret: string;
    scope: string;
}

@Injectable()
export class Environment {
    env: IEnvironment;

    constructor(@Inject(EnvVariables) environment) {
        this.env = environment;
    }
}