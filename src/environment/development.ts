import { IEnvironment } from './environment';

export const devVariables: IEnvironment = {
  apiEndpoint: 'http://localhost:3038/',
  environmentName: 'Development Environment',
  ionicEnvName: 'dev',

  client_id: 'ClientAPI',
  client_secret: 'pe7api!',
  grant_type: 'password',
  scope: 'api openid profile email'
};