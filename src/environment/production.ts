import { IEnvironment } from './environment';

export const prodVariables: IEnvironment = {
  apiEndpoint: 'https://pettracker.azurewebsites.net/',
  environmentName: 'Production Environment',
  ionicEnvName: 'prod',

  client_id: 'ClientAPI',
  client_secret: 'pe7api!',
  grant_type: 'password',
  scope: 'api openid profile email'
};