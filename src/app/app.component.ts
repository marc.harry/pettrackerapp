import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';

import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { storeToken } from '../reducers/auth';

import { setActiveTracker } from '../pages/trackers/trackers.reducers';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, store: Store<AppState>, storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      storage.get('activeTracker').then(trackerId => {
        if (trackerId) {
          store.dispatch(setActiveTracker(trackerId));
        }
      });

      // Logic to correctly display login page or go directly into application.
      storage.get('authToken').then(authToken => {
        if (authToken) {
          store.dispatch(storeToken(JSON.parse(authToken)));
        }
        store.select(s => s.auth.token.access_token).subscribe(token => {
          if (token) {
            this.rootPage = TabsPage;
          } else {
            this.rootPage = LoginPage;
          }
        });
      });

      
    });
  }
}
