import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';

import {AuthenticationService} from '../services/authenticationService';

import {LOGIN, LOGOUT, REGISTER, storeToken} from '../reducers/auth';

@Injectable()
export class AuthEffects {
  constructor(
    private authenticationService: AuthenticationService,
    private actions$: Actions,
    private storage: Storage
  ) { }

  @Effect() login$ = this.actions$
      .ofType(LOGIN)
      .map(action => action.payload)
      .switchMap(payload => this.authenticationService.login(payload)
        .map(res => {
          const token = res.json();
          token.requested_date = Date.now();
          this.storage.set('authToken', JSON.stringify(token));
          return storeToken(token);
        })
        .catch(error => {
          console.log(error);
          return Observable.empty();
        })
      );

  @Effect() register$ = this.actions$
      .ofType(REGISTER)
      .map(action => action.payload)
      .switchMap(payload => this.authenticationService.register(payload)
        .map(res => {
          const token = res.json();
          token.requested_date = Date.now();
          this.storage.set('authToken', JSON.stringify(token));
          return storeToken(token);
        })
        .catch(error => {
          console.log(error);
          return Observable.empty();
        })
      );

  @Effect() logout$ = this.actions$
      .ofType(LOGOUT)
      .map(action => {
        this.storage.remove('authToken');
        return Observable.empty();
      });
}