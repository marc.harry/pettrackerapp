import { counter } from './counter';
import { auth, IAuthState } from './auth';

import { trackers, ITrackerState } from '../pages/trackers/trackers.reducers';

export interface AppState {
    counter: number;
    auth: IAuthState;
    trackers: ITrackerState;
}

export const reducers = {
    counter,
    auth,
    trackers
};
