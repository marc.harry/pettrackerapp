import { Action } from '@ngrx/store';

export const LOGIN = 'LOGIN';
export const STORE_USER  = 'STORE_USER';
export const STORE_TOKEN  = 'STORE_TOKEN';
export const LOGOUT  = 'LOGOUT';
export const REGISTER = 'REGISTER';

export const loginUser = (payload: {username: string, password: string}): Action => ({ type: LOGIN, payload });
export const storeToken = (payload: IAuthToken): Action => ({ type: STORE_TOKEN, payload });
export const registerUser = (payload: IRegisterUser): Action => ({ type: REGISTER, payload });
export const logoutUser = (): Action => ({ type: LOGOUT });

interface IRegisterUser {
    email: string;
    password: string;
    confirmPassword: string;
}

interface IAuthToken {
    access_token: string;
    expires_in: number;
    token_type: string;
    requested_date: Date;
}

export interface IAuthState { 
    user: any;
    token: IAuthToken;
 }

 const initialState: IAuthState = {
     user: null,
     token: {} as IAuthToken
 }

export function auth(state = initialState, action: Action) {
    switch (action.type) {
        case STORE_USER:
            return Object.assign({}, state, {user: action.payload});
        case STORE_TOKEN:
            return Object.assign({}, state, {token: action.payload});
        case LOGOUT:
            return initialState;
        default:
            return state;
    }
}