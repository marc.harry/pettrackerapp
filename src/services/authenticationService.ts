import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Environment } from '../environment/environment';

@Injectable()
export class AuthenticationService {
  constructor(
    private http: Http,
    private settings: Environment,

  ) {  }

  getBaseUrl() {
    return `${this.settings.env.apiEndpoint}connect/`;
  }

  getHeaders() {
    return new Headers({
      'Content-Type': 'application/json'
    });
  }

  login(settings: {
    client_id?: string;
    username: string;
    password: string;
    grant_type?: string;
    client_secret?: string;
    scope?: string;
  }) {
    const headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    if (!settings.client_id) {
      settings.client_id = this.settings.env.client_id;
    }
    if (!settings.client_secret) {
      settings.client_secret = this.settings.env.client_secret;
    }
    if (!settings.grant_type) {
      settings.grant_type = this.settings.env.grant_type;
    }
    if (!settings.scope) {
      settings.scope = this.settings.env.scope;
    }

    let body = 'grant_type=' + settings.grant_type
      +'&client_id=' + settings.client_id
      +'&client_secret=' + settings.client_secret
      +'&scope=' + settings.scope
      +'&username=' + settings.username
      +'&password=' + settings.password;

    return this.http.post(`${this.getBaseUrl()}token`, body, { headers });
  }

  register(details) {
    return this.http.post(`${this.settings.env.apiEndpoint}account/registration`, details, this.getHeaders());
  }
}